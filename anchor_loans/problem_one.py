from typing import Tuple, Iterable
from itertools import product


POSITIVE = '+'
NEGATIVE = '-'
NOTHING = 'n'
STRING_GENERATOR = POSITIVE + NEGATIVE + NOTHING

EXPECTED = 100


def generate_possibilities() -> Iterable[Tuple]:
    return product(STRING_GENERATOR, repeat=8)


def parse(combination: Tuple) -> str:
    return '1{}2{}3{}4{}5{}6{}7{}8{}9'.format(*combination).replace(NOTHING, '')


def match(possibility: str) -> bool:
    return eval(possibility) == EXPECTED


def execute() -> Iterable[str]:
    for possibility in generate_possibilities():
        parsed_possibility = parse(possibility)
        if match(parsed_possibility):
            yield parsed_possibility + '={}'.format(EXPECTED)

if __name__ == '__main__':
    print(*list(execute()), sep='\n')
