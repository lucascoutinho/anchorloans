from bottle import get, template

from anchor_loans import problem_one

__all__ = ('home',)


@get('/')
def home():
    return template('home', results=problem_one.execute())

