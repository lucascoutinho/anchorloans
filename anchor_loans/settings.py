import os

import bottle

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

bottle.TEMPLATE_PATH = [os.path.join(CURRENT_DIR,  'views')]
ROOT_ASSETS_DIR = os.path.join(CURRENT_DIR, 'assets/')

RELOADER = True
DEBUG = True


