#Challenge

**Resolution for problem one:**
_Write a program that outputs all possibilities to put + or - or nothing between the numbers 1, 2, ..., 9 (in this order) such that the result is always 100. For example: 1 + 2 + 34 – 5 + 67 – 8 + 9 = 100._

##Repository:

[Bitbucket](https://bitbucket.org/lucascoutinho/anchorloans)

##Installation requirements:

**python version 3.5**
**linux**

##Instructions:

**Optionally you can create a virtual environment.**

```
pip install -r requirements.txt
```

_Run:_

```
python app.py
```

_Run tests:_

```
python -m unittest -v
```

