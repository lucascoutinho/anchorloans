from bottle import run, default_app

from anchor_loans.settings import DEBUG, RELOADER

if __name__ == '__main__':
    run(host='localhost', port=8080, debug=DEBUG, reloader=RELOADER)
else:
    app = application = default_app()

