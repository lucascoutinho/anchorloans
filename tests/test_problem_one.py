from unittest import TestCase

from anchor_loans.problem_one import (
    parse, match, generate_possibilities, EXPECTED as EXPECTED_PROBLEM
)


class TestProblemOne(TestCase):

    def test_parser_positive(self):
        expected = '1+2+3+4+5+6+7+8+9'
        self.assertEqual(expected,
                         parse(('+', '+', '+', '+', '+', '+', '+', '+')))

    def test_parser_negative(self):
        expected = '1-2-3-4-5-6-7-8-9'
        self.assertEqual(expected,
                         parse(('-', '-', '-', '-', '-', '-', '-', '-')))

    def test_parser_nothing(self):
        expected = '123456789'
        self.assertEqual(expected,
                         parse(('n', 'n', 'n', 'n', 'n', 'n', 'n', 'n')))

    def test_match(self):
        self.assertTrue(EXPECTED_PROBLEM, match('99+1'))

    def test_generate_possibilities(self):
        possibilities = list(generate_possibilities())
        first_expected = ('+', '+', '+', '+', '+', '+', '+', '+')
        mid_expected = ('-', '-', '-', '-', '-', '-', '-', '-')
        last_expected = ('n', 'n', 'n', 'n', 'n', 'n', 'n', 'n')
        self.assertEqual(first_expected, possibilities[0])
        self.assertEqual(mid_expected, possibilities[3280])
        self.assertEqual(last_expected, possibilities[-1])

